$(document).ready(function () {

	var JSAPP = JSAPP || {};
	var body = $('body');

	JSAPP.menu = function () {
		$('#btn-menu').on('click', function () {
			if (!body.hasClass('menu-opened')) {
				body.addClass('menu-opened');
			}

			return false;
		});
		$('#btn-close-menu').on('click', function () {
			body.removeClass('menu-opened');

			return false;
		});
	};

	JSAPP.scrollMenu = function () {
		$('.menu a').click(function (e) {
			var anchor = $(this);
			$('#btn-close-menu').trigger('click');

			$('html, body').stop().animate({
				scrollTop: $(anchor.attr('href')).offset().top
			}, 1000);

			e.preventDefault();
		});
		return false;
	};

	JSAPP.headingMain = function() {
		var count_slides = $('#main .slide').size();
		var current_slide = -1;
		var is_hover = false;

		$('.slide').hover(function () {
			is_hover = true;
		}, function () {
			is_hover = false;
		});

		function next_slide() {
			if (++current_slide > count_slides - 1) {
				current_slide = 0;
			}
			$('.slide').removeClass('active');
			$('.slide'+ current_slide).addClass('active');
		}

		setInterval(function(){
			if (!is_hover) {
				next_slide();
			}
		}, 5000);

		next_slide();
	};

	JSAPP.animationOnScroll = function () {


		//animation on scroll

		var $window = $(window),
			win_height_padded = $window.height() * 0.6;
		//isTouch = Modernizr.touch;
		if ($window.width() > 480) {
			//			if (isTouch) {
			//				$('.revealOnScroll').addClass('animated');
			//			}


			function reAnimation() {
				var scrolled = $window.scrollTop(),
					wh = $window.height(),
					win_height_padded = wh * 1.1;


				// Showed...
				$(".re-am:not(.animated)").each(function () {
					var $this = $(this),
						offsetTop = $this.offset().top;

					var triggerTop = win_height_padded;

					if ($this.data('triggertop')) {
						triggerTop = wh + 1 * $this.data('triggertop');
					}

					if (scrolled + triggerTop > offsetTop) {
						if ($this.data('timeout')) {
							window.setTimeout(function () {
								$this.addClass('animated ');
							}, parseInt($this.data('timeout'), 10));
						} else {
							$this.addClass('animated ' + $this.data('animation'));
						}
					}
				});
				// Hidden...
				$(".re-am.animated").each(function (index) {
					var $this = $(this),
						offsetTop = $this.offset().top;
					var triggerTop = win_height_padded;

					if ($this.data('triggertop')) {
						triggerTop = wh + 1 * $this.data('triggertop');
					}

					if (scrolled + triggerTop < offsetTop) {
						$(this).removeClass('animated ' + $this.data('animation'));
					}
				});
			}

			reAnimation();
			$window.on('scroll', reAnimation);
		}
	};

	JSAPP.quantityTimer = function () {
		var container = $('#general-info');

		function qtyTimer() {
			$('#general-info .qty').each(function () {
				var el = $(this);
				var num = parseInt(el.html());
				container.addClass('processed');

				function addIt(i) {
					if (i + 5 <= num) {
						i += 5;
						el.html(i + '+');
						setTimeout(function () {
							addIt(i);
						}, 25);
					}
				}
				addIt(0);
			});
		}

		function resetTimer() {
			var scrolled = $(window).scrollTop(),
				wh = $(window).height(),
				el_offset = $('#general-info').offset().top;

			if (el_offset <= scrolled <= (el_offset + wh) && !container.hasClass('processed')) {
				qtyTimer();

			} else if (el_offset > scrolled > (el_offset + wh) && container.hasClass('processed')) {
				container.removeClass('processed');
			}
		}
		//resetTimer();
		$(window).scroll(function () {
			resetTimer();
		});
	};

	JSAPP.coreBlockAnimations = function() {
		var context = $('#core-capabilities');
        var ww = $(window).width();
        if(ww <= 991) {
            $('.web', context).removeClass('delay-a1500').addClass('delay-a500');
            $('.sforce', context).removeClass('delay-a500').addClass('delay-a500');
            $('.mobile', context).removeClass('delay-a2500').addClass('delay-a500');
        }
		$(window).on('resize', function() {
			var ww = $(window).width();
			if(ww <= 991) {
                $('.web', context).removeClass('delay-a1500').addClass('delay-a500');
                $('.sforce', context).removeClass('delay-a500').addClass('delay-a500');
                $('.mobile', context).removeClass('delay-a2500').addClass('delay-a500');
			} else {
                $('.web', context).removeClass('delay-a500').addClass('delay-a1500');
                $('.sforce', context).removeClass('delay-a500').addClass('delay-a500');
                $('.mobile', context).removeClass('delay-a500').addClass('delay-a2500');
            }
		});
	};

	JSAPP.feedbackFormCall = function () {
		$('#feedback-form,#blanket, #popUpDiv').bind('mousewheel DOMMouseScroll', function (e) {
			var scrollTo = null;

			if (e.type == 'mousewheel') {
				scrollTo = (e.originalEvent.wheelDelta * -1);
			} else if (e.type == 'DOMMouseScroll') {
				scrollTo = 40 * e.originalEvent.detail;
			}

			if (scrollTo) {
				e.preventDefault();
				$(this).scrollTop(scrollTo + $(this).scrollTop());
			}
		});
		$("#blanket,.popup_button").click(function () {
			$("#feedback-form").toggle(function () {
				if ($(this).is(':visible')) {
					$("#feedback-form").css('display', 'block');
				} else if ($(this).is(':hidden')) {
					$("#feedback-form").css('display', 'none');
				}
			});

			return false;
		});
	};

	JSAPP.feedbackForm = function () {

		$('#form1').ajaxForm({
			//crossDomain: true,
			//dataType: 'jsonp',
			beforeSubmit: function (formData, jqForm, options) {
				var form = jqForm[0];
				var err = false;
				$(form).find('.error').removeClass('error');
				if (!form.first_name.value) {
					$(form.first_name).addClass('error');
					err = true;
				}
				if (!form.last_name.value) {
					$(form.last_name).addClass('error');
					err = true;
				}
				if (!form.email.value) {
					$(form.email).addClass('error');
					err = true;
				} else {
					if (!validateEmail(form.email.value)) {
						$(form.email).addClass('error');
						err = true;
					}
				}

				if (!form.phone.value) {
					$(form.phone).addClass('error');
					err = true;
				}
				if (!form.company.value) {
					$(form.company).addClass('error');
					err = true;
				}

				if (!form.subject.value) {
					$(form.subject).addClass('error');
					err = true;
				}

				if (err) {
					return false;
				}
			},
			success: function () {
				//$('.fb-form-wrap').fadeOut();
				//$('.fb-sec-wrap').fadeIn();
				$('#form1').fadeOut();
				$('.success').fadeIn();
			},
			error: function (e) {
				if (e.status == 200) {
					$('#form1').fadeOut();
					$('.success').fadeIn();
				} else {
					console.log(e);
					console.log('error');
				}
			}
		});
		$('#form1 input, #form1 textarea').change(function () {
			$(this).removeClass('error');
		});
	};

	JSAPP.initMap = function () {
		var myLatLng = {
			lat: 40.7654536,
			lng: -73.9828878
		};
		var map = new google.maps.Map(document.getElementById('map'), {
			center: myLatLng,
			scrollwheel: false,
			zoom: 10,
			mapTypeControl: false,
			disableDefaultUI: true
		});
		//	var marker = new google.maps.Marker({
		//		map: map,
		//		position: myLatLng,
		//		title: 'Customertimes'
		//	});
	};

	JSAPP.clientsCarouselDesktop = function () {
		var owl = $("#clients-carousel");
		owl.owlCarousel({
			itemsCustom: [
				//[0, 1],
				//[450, 1],
				[600, 2],
				[700, 3],
				[1000, 4],
				[1200, 4],
				[1400, 5],
				[1600, 5]
			],
			navigation: true,
			mouseDrag: true,
			autoPlay : true,
			stopOnHover : true
		});
	};

	JSAPP.clientsCarouselMobile = function () {
		var owl = $("#clients-carousel-mobile");
		owl.owlCarousel({
			itemsCustom: [
				[0, 1]
			],
			navigation: true,
			mouseDrag: true,
			autoPlay : true,
			stopOnHover : true
		});
	};

	JSAPP.menu();
	JSAPP.headingMain();
	JSAPP.scrollMenu();
	JSAPP.animationOnScroll();
	JSAPP.quantityTimer();
	JSAPP.coreBlockAnimations();
	JSAPP.feedbackFormCall();
	JSAPP.feedbackForm();
	JSAPP.initMap();
	var $window = $(window);
	if($window.width() > 620) {
		JSAPP.clientsCarouselDesktop();
	} else {
		JSAPP.clientsCarouselMobile();
	}
	$window.on('resize', function() {
		if($window.width() > 620) {
			JSAPP.clientsCarouselDesktop();
		} else {
			JSAPP.clientsCarouselMobile();
		}
	});
});

function validateEmail(emailaddress) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if (emailReg.test(emailaddress)) {
		return true;
	}
	return false;
}