var gulp = require('gulp'),
    watch = require('gulp-watch'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rigger = require('gulp-rigger');

var browserSyncConfig = {
    host: 'localhost',
    port: 3000,
    tunnel: false,
    server: {
        baseDir: "./"
    }
};

var autopref_conf = {
    browsers: ['last 10 versions'],
    cascade: false
};

browserSync.create(browserSyncConfig);


var path = {
    src: {
        html: 'index.html',
        scss: 'dev/scss/style.scss',
        img: 'dev/img/**/*.*',
        vendor_css: 'dev/vendor/**/*.css',
        vendor_js: 'dev/vendor/**/*.js'
    },
    dest: {
        css: 'assets/css/',
        js: 'assets/js/',
        img: 'assets/img/',
        //vendor: 'dev/vendor/compiled/'
    },
    watch: {
        html: 'index.html',
        style: 'dev/scss/**/*.scss',
        img: 'dev/img/*.*'
    }
}

// ========== Html ============
gulp.task('html', function () {
    gulp.src(path.src.html)
        .pipe(browserSync.reload({
            stream: true
        }))
});

// ========== Build vendor css ============
gulp.task('build:vendor', function () {
    gulp.src(path.src.vendor_css)
        .pipe(plumber())
        .pipe(minifycss())
        .pipe(gulp.dest(path.dest.css));

    gulp.src(path.src.vendor_js)
        .pipe(plumber())
        .pipe(rigger())
        //.pipe(uglify())
        .pipe(gulp.dest(path.dest.js))
});

// ============== CSS ===================
gulp.task('build:css', function () {
    gulp.src(path.src.scss)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer(autopref_conf))
        .pipe(sourcemaps.write('.'))
        //.pipe(minifycss())
        .pipe(gulp.dest(path.dest.css))
        .pipe(browserSync.reload({
            stream: true
        }));
});

// ============== IMAGES =================
gulp.task('build:image', function () {
    gulp.src(path.src.img)
        .pipe(plumber())
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(path.dest.img))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('watch', function () {
    watch(path.watch.html, function () {
        gulp.start('html');
    });
    watch(path.watch.style, function () {
        gulp.start('build:css');
    });
    watch(path.watch.img, function () {
        gulp.start('build:image');
    });

});

// ============== OTHER =================
gulp.task('webserver', function () {
    browserSync(browserSyncConfig);
});

gulp.task('build', ['build:vendor', 'build:css']);
gulp.task('server', ['watch', 'webserver']);
gulp.task('default', ['build:vendor', 'build:css', 'build:image', 'watch', 'webserver']);
